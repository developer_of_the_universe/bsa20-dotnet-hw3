﻿using System;
using System.Threading.Tasks;

namespace CoolParking.PL
{
    class Program
    {        
        static async Task Main(string[] args)
        {            
            try
            {
                await Application.Run();
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
            catch (Exception)
            {
                Console.WriteLine("Server is offline, please, try again later");
            }
        }
    }
}
