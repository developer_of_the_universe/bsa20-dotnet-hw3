﻿using System;
using System.Threading.Tasks;

namespace CoolParking.PL
{
    public static class Application
    {
        public async static Task Run()
        {
            while (true)
            {
                Console.Clear();
                PrintMainMenu();
                char input = InputHandler.GetCharFromUserByCondition(input => input == 'e' || input >= '1' || input <= '9');
                Console.Clear();
                if (input == 'e')
                {
                    Console.WriteLine("Thank you for using CoolParking. See you soon!)");
                    break;
                }
                string inputResult = await HandleInput(input);
                Console.Clear();
                Console.WriteLine(inputResult + "\n\nPress any key to continue");
                Console.ReadKey();
            }
        }
        private static void PrintMainMenu()
        {
            Console.WriteLine("Greetings from CoolParking! Please, select an option:\n\n"
                + "0. Check vehicle\n"
                + "1. Check parking balance\n"
                + "2. Check incomes for current period\n"
                + "3. Check amount of free spaces\n"
                + "4. View all transactions for current period\n"
                + "5. View history of transaction\n"
                + "6. Get list of all vehicles\n"
                + "7. Put your vehicle\n"
                + "8. Take your vehicle\n"
                + "9. Top up your balance\n"
                + "e. Exit\n");
        }
        private async static Task<string> HandleInput(char input) => input switch
        {
            '0' => await ParkingServiceHandler.GetVehicle(),
            '1' => await ParkingServiceHandler.GetBalance(),
            '2' => await ParkingServiceHandler.GetCurrentIncome(),
            '3' => await ParkingServiceHandler.GetFreePlacesInfo(),
            '4' => await ParkingServiceHandler.GetLastTransactions(),
            '5' => await ParkingServiceHandler.ReadFromLog(),
            '6' => await ParkingServiceHandler.GetVehicles(),
            '7' => await ParkingServiceHandler.AddVehicle(),
            '8' => await ParkingServiceHandler.RemoveVehicle(),
            '9' => await ParkingServiceHandler.TopUpBalance(),
            _ => "Something went wrong"
        };
    }
}
