﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.PL
{
    static class ParkingServiceHandler
    {
        static readonly HttpClient client = new HttpClient();
        static bool GoToMainMenu = false;
        static ParkingServiceHandler()
        {
            client.BaseAddress = new Uri("http://localhost:5000/api/");
            client.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        internal async static Task<string> GetBalance()
        {            
           return "Balance of Parking is " + await client.GetStringAsync("parking/balance");    
        }    
        internal async static Task<string> GetCurrentIncome()
        {
            string responseBody = await client.GetStringAsync("transactions/last");
            ReadOnlyCollection<TransactionInfo> transactions = 
                JsonConvert.DeserializeObject<ReadOnlyCollection<TransactionInfo>>(responseBody);
            decimal income = 0;
            if (transactions != null && transactions.Any())                
                income = transactions.Select(tr => tr.Sum).Sum();
            return "Current income of Parking is " + income;
        }
        internal async static Task<string> GetFreePlacesInfo()
        {
            string free = await client.GetStringAsync("parking/freePlaces");
            string capacity = await client.GetStringAsync("parking/capacity");
            return $"Free {free} places of {capacity}";
        }
        internal async static Task<string> GetLastTransactions()
        {            
            string responseBody = await client.GetStringAsync("transactions/last");
            ReadOnlyCollection<TransactionInfo> transactions =
                JsonConvert.DeserializeObject<ReadOnlyCollection<TransactionInfo>>(responseBody);
            if (transactions == null || !transactions.Any())
                return "There are no current transactions now";
            string result = string.Join("\n", transactions.Select(tr => tr.ToString()));
            return "List of transactions:\n" + result;
        }
        internal async static Task<string> ReadFromLog()
        {            
            HttpResponseMessage response = await client.GetAsync("transactions/all");
            if (response.StatusCode == HttpStatusCode.NotFound)
                return "Server error: file not found";
            string result = JsonConvert.DeserializeObject<string>(await response.Content.ReadAsStringAsync());
            if (string.IsNullOrWhiteSpace(result))
                return "There are no transactions in the log now";
            return "Transactions from log:\n" + result;                                   
        }
        internal async static Task<string> GetVehicle()
        {
            GoToMainMenu = false;
            string id = GetVehicleIdFromUser();
            if (GoToMainMenu)
                return "Operation canceled";
            HttpResponseMessage response = await client.GetAsync("vehicles/" + id);
            if (response.StatusCode == HttpStatusCode.BadRequest)
                return "Incorrect id";
            if(response.StatusCode == HttpStatusCode.NotFound)
                return "There are no such vehicle";
            try
            {
                Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(await response.Content.ReadAsStringAsync());
                if (vehicle == null)
                    return "There are no such vehicle";
                return vehicle.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return ex.Message;
            }
        }
        internal async static Task<string> GetVehicles()
        {            
            string responseBody = await client.GetStringAsync("vehicles");
            try
            {
                List<Vehicle> vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(responseBody);
                if (!vehicles.Any())
                    return "There are no vehicles yet";
                string separator = "\n============================\n";
                string result = string.Join(separator, vehicles.Select(vehicle => vehicle.ToString()));
                return "All vehicles:\n\n" + result;
            }
            catch (Exception)
            {
                return "Server error, please, contact admin";
            }                        
        }
        internal async static Task<string> AddVehicle()
        {
            string responseBody = await client.GetStringAsync("parking/freePlaces");
            int free;
            if (!int.TryParse(responseBody, out free) || free == 0)
                return "There is no place, try again later";
            GoToMainMenu = false;
            const string cancel = "Operation canceled";
            string id = CreateVehicleId();
            if (GoToMainMenu)
                return cancel;
            VehicleType type = GetVehicleType();
            if (GoToMainMenu)
                return cancel;
            decimal balance = GetSumForBalance();
            if (GoToMainMenu)
                return cancel;
            try
            {
                Vehicle vehicle = new Vehicle(id, type, balance);
                string requestBody = JsonConvert.SerializeObject(vehicle);                
                HttpResponseMessage response = await client.PostAsync("vehicles", 
                    new StringContent(requestBody, Encoding.UTF8, "application/json"));
                if (response.StatusCode == HttpStatusCode.BadRequest)
                    return "Incorrect input. Try again";
                if(response.StatusCode == HttpStatusCode.InternalServerError)
                    return "Server error, please, contact admin";
                return vehicle.ToString();
            }
            catch (Exception ex)
            {
                return "Failed:\n" + ex.Message;
            }
        }
        internal async static Task<string> RemoveVehicle()
        {
            GoToMainMenu = false;
            string id = GetVehicleIdFromUser();
            if (GoToMainMenu)
                return "Operation canceled";            
            HttpResponseMessage response = await client.DeleteAsync("vehicles/" + id);
            return response.StatusCode switch 
            {
                HttpStatusCode.BadRequest => "Incorrect vehicle id. Try again",
                HttpStatusCode.PaymentRequired => "Need payment before taking vehicle",
                HttpStatusCode.NotFound => "This vehicle isn't here",
                _ => $"Successfully removed { id }"
            };            
        }
        internal async static Task<string> TopUpBalance()
        {
            GoToMainMenu = false;
            string id = GetVehicleIdFromUser();
            if (GoToMainMenu)
                return "Operation canceled";
            decimal Sum = GetSumForBalance();
            if (GoToMainMenu)
                return "Operation canceled";
            try
            {
                StringContent requestBody = new StringContent("");
                    //JsonConvert.SerializeObject(new { id, Sum }));
                HttpResponseMessage response = await client.PutAsync("transactions/topUpVehicle", requestBody);
                return response.StatusCode switch
                {
                    HttpStatusCode.BadRequest => "Incorrect vehicle id. Try again",
                    HttpStatusCode.NotFound => "This vehicle isn't here",
                    _ => $"Topped up {id} on {Sum}"
                };
            }
            catch (ArgumentException ex)
            {
                return "Failed:\n" + ex.Message;
            }
        }
        private static string CreateVehicleId()
        {
            string randomCase = "random";
            string goBackCase = "back";
            Console.Clear();
            Console.WriteLine("Enter id of your vehicle to park(in format XX-YYYY-XX)\n"
                + $"Or {randomCase} to generate it\n"
                + $"Or {goBackCase} to go back\n");
            string result = InputHandler.GetStringFromUser();
            if (result == goBackCase)
                GoToMainMenu = true;
            return result != randomCase ? result : Vehicle.GenerateRandomRegistrationPlateNumber();
        }
        private static VehicleType GetVehicleType()
        {
            Console.Clear();
            Console.WriteLine("Choose type of your vehicle:\n"
                + "1. PassengerCar\n"
                + "2. Truck\n"
                + "3. Bus\n"
                + "4. Motorcycle\n"
                + "b. Cancel\n");
            char input = InputHandler.GetCharFromUserByCondition(input => input == 'b' || input >= '1' && input <= '4');
            if (input == 'b')
            {
                GoToMainMenu = true;
                return default;
            }
            return input switch
            {
                '1' => VehicleType.PassengerCar,
                '2' => VehicleType.Truck,
                '3' => VehicleType.Bus,
                '4' => VehicleType.Motorcycle,
                _ => throw new ArgumentException("Impossible value is possible)")
            };
        }
        private static decimal GetSumForBalance()
        {
            Console.Clear();
            Console.WriteLine("Enter sum for adding to your balance (greater than 0) or -1 to Cancel:\n");
            decimal result = InputHandler.GetDecimalFromUserByCondition(input => input == -1 || input > 0);
            if (result == -1)
                GoToMainMenu = true;
            return result;
        }
        private static string GetVehicleIdFromUser()
        {
            string goBackCase = "back";
            Console.Clear();
            Console.WriteLine($"Enter id of vehicle (XX-YYYY-XX) or \"{goBackCase}\" to go back: ");
            string id = InputHandler.GetStringFromUser();
            if (id == goBackCase)
                GoToMainMenu = true;
            return id;
        }        
    }
}