﻿using System;

namespace CoolParking.PL
{
    internal static class InputHandler
    {
        internal static char GetCharFromUserByCondition(Predicate<char> IsCorrectInput)
        {
            while (true)
            {
                char input = Console.ReadKey().KeyChar;
                if (IsCorrectInput(input))
                    return input;
            }
        }
        internal static decimal GetDecimalFromUserByCondition(Predicate<decimal> IsCorrectInput)
        {
            while (true)
            {
                string input = Console.ReadLine();
                if (decimal.TryParse(input, out decimal result) && IsCorrectInput(result))
                    return result;
            }
        }
        internal static string GetStringFromUser()
        {
            return Console.ReadLine();
        }
    }
}