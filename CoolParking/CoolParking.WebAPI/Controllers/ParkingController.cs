﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService parkingService;
        public ParkingController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(parkingService.GetBalance()); 
        }
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(parkingService.GetCapacity());
        }
        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(parkingService.GetFreePlaces());
        }
    }
}
