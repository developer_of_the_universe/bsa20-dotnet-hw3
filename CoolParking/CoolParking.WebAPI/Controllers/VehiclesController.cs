﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Net;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IParkingService parkingService;
        public VehiclesController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        [HttpGet]
        public ActionResult<string> Get() 
        {
            return Ok(parkingService.GetVehicles());
        }
        [HttpGet("{id}")]
        public ActionResult<string> Get(string id) 
        {            
            if (!parkingService.IsCorrectVehicleId(id))
                return BadRequest();
            try
            {
                Vehicle vehicle = parkingService.GetVehicle(id);                
                return Ok(vehicle);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
        }
        [HttpPost]
        public ActionResult Post([FromBody]string vehicleJson) 
        {
            try
            {
                Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(vehicleJson);                
                if (vehicle == null)
                    return BadRequest();               
                parkingService.AddVehicle(vehicle);
                return StatusCode((int)HttpStatusCode.Created, vehicle);                
            }
            catch (JsonException)
            {
                return BadRequest();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            catch (InvalidOperationException)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }                 
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id) 
        {
            if (!Vehicle.IsCorrectId(id))
                return BadRequest();
            try
            {
                parkingService.RemoveVehicle(id);
                return StatusCode((int)HttpStatusCode.NoContent); 
            }
            catch(ArgumentException)
            {
                return NotFound();
            }
            catch(InvalidOperationException)
            {
                return StatusCode((int)HttpStatusCode.PaymentRequired);
            }
        }
    }
}
