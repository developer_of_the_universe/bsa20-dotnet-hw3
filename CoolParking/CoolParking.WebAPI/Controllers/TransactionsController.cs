﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService parkingService;
        public TransactionsController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        [HttpGet("last")]
        public ActionResult<string> GetLast()
        {
            TransactionInfo[] transactions = parkingService.GetLastParkingTransactions();
            return Ok(transactions);
        }
        [HttpGet("all")]
        public ActionResult<string> GetAll()
        {
            try
            {
                string result = parkingService.ReadFromLog();
                return Ok(result);
            }
            catch (InvalidOperationException)
            {
                return NotFound();                
            }
        }
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUp([FromBody]string request)
        {
            var body = new { id = "", Sum = 0.0m };
            try
            {
                body = JsonConvert.DeserializeAnonymousType(request, body);
                if (body == null || body.id == null || !Vehicle.IsCorrectId(body.id) || body.Sum <= 0)
                    return BadRequest();                
                parkingService.TopUpVehicle(body.id, body.Sum);
                Vehicle vehicle = parkingService.GetVehicle(body.id);
                return Ok(vehicle);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            catch (JsonException)
            {
                return BadRequest();
            }
        }
    }
}