using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StringBinderExtensionForController;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services
                .AddMvc(options =>
                {
                    options.InputFormatters.Insert(0, new RawStringBodyInputFormatter());
                });
            services.AddSingleton<IParkingService, ParkingService>(sp => {
                ITimerService withdrawTimer = new TimerService();
                ITimerService logTimer = new TimerService();
                ILogService logger = new LogService("Transactions.log");
                return new ParkingService(withdrawTimer, logTimer, logger);
            });
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }                      

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
