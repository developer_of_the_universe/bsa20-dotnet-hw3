﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        double interval;
        public double Interval {
            get => interval > 0 ? interval : 1000;
            set => interval = value * 1000;                            
        }
        private Timer timer;
        public event ElapsedEventHandler Elapsed;
                              
        public void Start()
        {                        
            CreateTimer();      
            timer.Start();          
        }
        public void Stop()
        {
            timer.Stop();
        }
        private void CreateTimer()
        {            
            timer = new Timer(Interval);            
            timer.Elapsed += Elapsed;
            timer.AutoReset = true;
        }                
        public void Dispose()
        {
            timer.Dispose();
        }
    }
}