﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal Balance = 0;
        public static int Capacity = 10;
        public static double TransactionPeriod = 5;
        public static double LogPeriod = 60;
        public static Dictionary<VehicleType, decimal> Tarriffs;
        public static decimal PenaltyRatio = 2.5M;
        static Settings()
        {
            Tarriffs = new Dictionary<VehicleType, decimal>(4);
            Tarriffs[VehicleType.PassengerCar] = 2;
            Tarriffs[VehicleType.Truck] = 5;
            Tarriffs[VehicleType.Bus] = 3.5M;
            Tarriffs[VehicleType.Motorcycle] = 1;
        }
    }
}