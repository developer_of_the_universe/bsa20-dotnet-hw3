﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string VehicleId { get; set; }
        public DateTime OperationTime { get; set; }
        public decimal Sum { get; set; }

        public TransactionInfo(string vehicleId, decimal sum, DateTime operationTime)
        {
            VehicleId = vehicleId;
            OperationTime = operationTime;
            Sum = sum;
        }
        public TransactionInfo(string vehicleId, decimal sum) : this(vehicleId, sum, DateTime.Now) { }
        public override string ToString()
        {
            return OperationTime.ToString("G") + $" {VehicleId} paid {Sum}";
        }
    }
}